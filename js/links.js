// 随机排列
function shuffle(arr) {
  let i = arr.length;
  while (i) {
      let j = Math.floor(Math.random() * i--);
      [arr[j], arr[i]] = [arr[i], arr[j]];
  }
}
// 渲染数据
function renderlink(data) {
  var name, avatar, site, content = "";
  shuffle(data);
  for (var i = 0; i < data.length; i++) {
      name = data[i].name;
      url = data[i].url;
      avatar = data[i].avatar;
      slogan = data[i].slogan;
      content+='<div class="link-grid-container"><object class="link-grid-image" data="https://friends.shiyunhong.com/img/'+avatar+'"></object><p>'+name+'</p><p>'+slogan+'</p><a target="_blank" rel="noopener" href="'+url+'"></a></div>';
  }
  document.getElementsByClassName('link-grid')[0].innerHTML = content;
}
fetch('https://friends.shiyunhong.com/links.json').then(response => response.json()).then(data => renderlink(data));